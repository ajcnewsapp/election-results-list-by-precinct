# Live Election Results by Precinct
Live link: https://www.ajc.com/news/state--regional-govt--politics/results-precinct-2018-gubernatorial-election/bfLGO9ZDGMRz79YvTXPTXK/
This app takes a data file of the results of voting precincts and displays them by county. 

Use [precinct-results-scrape](https://bitbucket.org/ajcnewsapp/precinct_restults_scrape/src/master/) to get a list of county results pages from which to download the results, then paste into the url array of [this](https://bitbucket.org/ajcnewsapp/precinct-results-map/src/develop/data_cleaning/download.py), and run the python script to scrape the pages for the results. Take the generated `data_cleaning/votes_data.csv` file and load it into this project. 


#Author
THis was created by Jacquelyn Elias in November 2018